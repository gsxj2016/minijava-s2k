package minijava.s2k.test;

import minijava.s2k.codegen.CodeGen;
import minijava.s2k.codegen.GenState;
import minijava.s2k.jtb.ParseException;
import minijava.s2k.jtb.SpigletParser;
import minijava.s2k.jtb.syntaxtree.Node;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.stream.Collectors;

public class S2KTest {
    private File pgi;
    private File kgi;
    private File kgp;

    public S2KTest() throws IOException {
        pgi = File.createTempFile("pgi", ".jar");
        kgi = File.createTempFile("kgi", ".jar");
        kgp = File.createTempFile("kgi-patched", ".jar");
        pgi.deleteOnExit();
        kgi.deleteOnExit();
        kgp.deleteOnExit();
        try (InputStream pgiRes = getClass().getResourceAsStream("/pgi.jar")) {
            Files.copy(pgiRes, pgi.getAbsoluteFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
        try (InputStream kgiRes = getClass().getResourceAsStream("/kgi.jar")) {
            Files.copy(kgiRes, kgi.getAbsoluteFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
        try (InputStream kgpRes = getClass().getResourceAsStream("/kgi-patched.jar")) {
            Files.copy(kgpRes, kgp.getAbsoluteFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
        System.out.printf("(S)Piglet interpreter: %s\n", pgi.getAbsolutePath());
        System.out.printf("Kanga interpreter: %s\n", kgi.getAbsolutePath());
        System.out.printf("Patched Kanga interpreter: %s\n", kgp.getAbsolutePath());
    }

    private String genKanga(String resName) throws IOException, ParseException {
        try (InputStream in = getClass().getResourceAsStream(resName)) {
            Node root = new SpigletParser(in).Goal();
            GenState state = new GenState();
            root.accept(new CodeGen(), state);
            state.genCode();
            return state.code.toString();
        }
    }

    private String getSPiglet(String resName) throws IOException {
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(getClass().getResourceAsStream(resName)))) {
            return reader.lines().collect(Collectors.joining("\n"));
        }
    }

    private String runInterpreter(String code, File f) throws IOException {
        System.out.print(" ** code len ");
        System.out.println(code.length());
        Process process = Runtime.getRuntime().exec(new String[]{"java", "-jar", f.getAbsolutePath()});
        PrintWriter writer = new PrintWriter(process.getOutputStream());
        writer.print(code);
        writer.close();

        String error;
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(process.getErrorStream()))) {
            error = reader.lines().collect(Collectors.joining("\n"));
        }

        String output;
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(process.getInputStream()))) {
            output = reader.lines().collect(Collectors.joining("\n"));
        }

        while (process.isAlive()) {
            try {
                process.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (error.length() > 0) return null;
        return output;
    }

    private void testFile(boolean usePatchedKgi, String resName) throws IOException, ParseException {
        System.out.print("Testing with file: ");
        System.out.println(resName);
        System.out.println("Use " + (usePatchedKgi ? kgp : kgi).getName());
        String sCode = getSPiglet(resName);
        String kCode = genKanga(resName);
        System.out.println(" * run s");
        String ans = runInterpreter(sCode, pgi);
        Assert.assertNotNull(ans);
        System.out.println(" * run k");
        String out = runInterpreter(kCode, usePatchedKgi ? kgp : kgi);
        Assert.assertNotNull(out);
        if (ans.equals(out)) {
            System.out.println("Output is identical.");
        } else {
            System.out.println("## OUTPUT");
            System.out.println(out);
            System.out.println("## EXPECTED");
            System.out.println(ans);
            System.out.println("########");
            Assert.fail();
        }
        System.out.println();
    }

    @Test
    public void uclaTestCases() throws IOException, ParseException {
        testFile(false, "/test_ucla/QuickSort.spg");
        testFile(false, "/test_ucla/MoreThan4.spg");
        testFile(false, "/test_ucla/BinaryTree.spg");
        testFile(false, "/test_ucla/BubbleSort.spg");
        testFile(false, "/test_ucla/TreeVisitor.spg");
        testFile(false, "/test_ucla/LinearSearch.spg");
        testFile(false, "/test_ucla/LinkedList.spg");
        testFile(false, "/test_ucla/Factorial.spg");
    }

    @Test
    public void uclaOtherTestCases() throws IOException, ParseException {
        testFile(false, "/test_ucla_other/1-Basic.spg");
        testFile(false, "/test_ucla_other/2-Call.spg");
    }

    @Test
    public void oldTestCases() throws IOException, ParseException {
        testFile(true, "/test_old/QuickSort.spg");
        testFile(true, "/test_old/MoreThan4.spg");
        testFile(true, "/test_old/BinaryTree.spg");
        testFile(true, "/test_old/BubbleSort.spg");
        testFile(true, "/test_old/TreeVisitor.spg");
        testFile(true, "/test_old/LinearSearch.spg");
        testFile(true, "/test_old/LinkedList.spg");
        testFile(true, "/test_old/Factorial.spg");
        testFile(true, "/test_old/1-PrintLiteral.spg");
        testFile(true, "/test_old/2-Add.spg");
        testFile(true, "/test_old/3-Call.spg");
        testFile(true, "/test_old/4-Vars.spg");
        testFile(true, "/test_old/5-OutOfBounds.spg");
        testFile(true, "/test_old/array.spg");
        testFile(true, "/test_old/arrayOut.spg");
        testFile(true, "/test_old/arrayOut2.spg");
        testFile(true, "/test_old/arrayRef.spg");
        testFile(true, "/test_old/arrayNull.spg");
        testFile(true, "/test_old/arrayFetchNull.spg");
        testFile(true, "/test_old/arrayLenNull.spg");
        testFile(true, "/test_old/arrayNeg.spg");
        testFile(true, "/test_old/arrayNeg2.spg");
        testFile(true, "/test_old/zeroLenArray.spg");
        testFile(true, "/test_old/badArrayAlloc.spg");
        testFile(true, "/test_old/nestedCall.spg");
        testFile(true, "/test_old/emptyClass.spg");
        testFile(true, "/test_old/calc.spg");
        testFile(true, "/test_old/override.spg");
        testFile(true, "/test_old/callNull.spg");
        testFile(true, "/test_old/shortcut.spg");
        testFile(true, "/test_old/evalSeq.spg");
        testFile(true, "/test_old/loop.spg");
    }

    @Test
    public void myTestCases() throws IOException, ParseException {
        testFile(false, "/test_my/spill.spg");
        testFile(false, "/test_my/callerSave.spg");
        testFile(false, "/test_my/emptyFun.spg");
        testFile(false, "/test_my/sameLabel.spg");
        testFile(false, "/test_my/sameLabelUsed.spg");
        testFile(false, "/test_my/unused.spg");
        testFile(false, "/test_my/unusedLabel.spg");
        testFile(false, "/test_my/moreCallerSave.spg");
        testFile(false, "/test_my/muchSave.spg");
    }
}
