package minijava.s2k.codegen;

import minijava.s2k.logger.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class GenState {
    public Func currentFunc = null;
    public final List<Func> fn = new ArrayList<>();
    public boolean requireExp = false;

    private Integer lastTemp = null;
    private Integer moveDest = null;
    private String lastImm = null;

    private int labelId = 0;
    private final Map<String, String> labelMap = new HashMap<>();

    public int indentDepth = 0;
    private boolean hideIndent = false;
    public final StringBuilder code = new StringBuilder();

    private void generateIndent() {
        if (!hideIndent) {
            IntStream.range(0, indentDepth).forEach(x -> code.append("    "));
        }
    }

    public void generateCode(String codeText) {
        generateIndent();
        code.append(codeText);
        code.append('\n');
        hideIndent = false;
    }

    public void genCode() {
        for (Func f : fn) {
            Logger.log("==== Processing function " + f.name);
            int pass = 0;
            do {
                Logger.log("** Control/Data flow checking & optimization, pass " + (++pass));
                f.cleanUnreachableCode();
                f.checkControlFlow();
                f.checkActiveTemps();
            } while (f.cleanUnusedStatements());
            f.checkLiveRange();
            f.linearRegAlloc();
            f.genCode(this);
        }
    }

    public int getLastTemp() {
        if (lastTemp == null)
            throw new InternalCompilerError("missing temp");
        int result = lastTemp;
        lastTemp = null;
        return result;
    }

    public String getLastImm() {
        if (lastImm == null)
            throw new InternalCompilerError("missing constant");
        String result = lastImm;
        lastImm = null;
        return result;
    }

    public int getMoveDest() {
        if (moveDest == null)
            throw new InternalCompilerError("missing dest");
        int result = moveDest;
        moveDest = null;
        return result;
    }

    public boolean isImm() {
        if (lastImm == null && lastTemp == null)
            throw new InternalCompilerError("missing result");
        return lastImm != null;
    }

    public boolean isMove() {
        return moveDest != null;
    }

    public void ensureDestConsumed() {
        if (moveDest != null)
            throw new InternalCompilerError("dest not consumed");
    }

    public void setResultTemp(int temp) {
        if (lastTemp != null || lastImm != null)
            throw new InternalCompilerError("set result twice");
        lastTemp = temp;
    }

    public void setResultImm(String imm) {
        if (lastTemp != null || lastImm != null)
            throw new InternalCompilerError("set result twice");
        lastImm = imm;
    }

    public void setMoveDest(int temp) {
        if (moveDest != null)
            throw new InternalCompilerError("set dest twice");
        moveDest = temp;
    }

    public String globalLabel(String l) {
        String result = labelMap.get(l);
        if (result == null) {
            result = "La" + (labelId++);
            labelMap.put(l, result);
        }
        return result;
    }

    public void resetLabel() {
        labelMap.clear();
    }
}
