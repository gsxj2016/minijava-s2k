package minijava.s2k.codegen;

public class InternalCompilerError extends Error {
    public InternalCompilerError(String message) {
        super("[BUG] " + message);
    }
}
