package minijava.s2k.codegen;

import minijava.s2k.logger.Logger;
import minijava.s2k.statement.*;

import java.util.*;
import java.util.stream.IntStream;

public class Func {
    public final String name;
    public final ArrayList<Statement> statements = new ArrayList<>();
    public final Map<Integer, LiveRange> ranges = new HashMap<>();
    public final Map<Integer, String> regsMap = new HashMap<>();
    public final Map<String, Integer> saveMap = new HashMap<>();
    private final Map<Integer, Integer> spillMap = new HashMap<>();
    private final int argCount;
    private int spilledIdNext;
    private int saveIdNext = -999;
    private int maxArgCount = 0;

    public Func(String name, int argCount) {
        this.name = name;
        this.argCount = argCount;
        this.spilledIdNext = argCount > 4 ? argCount - 4 : 0;
    }

    public void cleanUnreachableCode() {
        if (statements.isEmpty()) return;
        statements.forEach(st -> st.reachable = false);
        Map<String, Integer> ls = new HashMap<>();
        int n = statements.size();
        for (int i = 0; i < n; ++i) {
            String la = statements.get(i).getStatementLabel();
            if (la != null) ls.put(la, i);
        }

        Queue<Integer> queue = new ArrayDeque<>();
        statements.get(0).reachable = true;
        queue.add(0);

        while (!queue.isEmpty()) {
            int sc = queue.poll();
            Statement st = statements.get(sc);
            String j = st.getJumpTarget();
            if (j != null) {
                int xc = ls.get(j);
                Statement xt = statements.get(xc);
                if (!xt.reachable) {
                    xt.reachable = true;
                    queue.add(xc);
                }
            }
            if (st.canRunNextStatement() && sc + 1 < n) {
                int xc = sc + 1;
                Statement xt = statements.get(xc);
                if (!xt.reachable) {
                    xt.reachable = true;
                    queue.add(xc);
                }
            }
        }

        statements.removeIf(st -> !st.reachable);
    }

    public void checkControlFlow() {
        Map<String, Statement> ls = new HashMap<>();
        statements.forEach(Statement::resetRunFroms);
        for (Statement st : statements) {
            String l = st.getStatementLabel();
            if (l != null) ls.put(l, st);
        }

        Statement last = null;
        for (Statement cur : statements) {
            if (last != null && last.canRunNextStatement()) {
                cur.addRunFrom(last);
            }
            String jt = cur.getJumpTarget();
            if (jt != null) {
                Statement tg = ls.get(jt);
                tg.addRunFrom(cur);
                tg.labelJumpReachable = true;
            }
            last = cur;
        }
    }

    public void checkActiveTemps() {
        Queue<Statement> queue = new ArrayDeque<>();
        for (Statement st : statements) {
            st.resetActiveTemps();
            st.requireResult = false;
            st.inQueue = true;
            queue.add(st);
        }
        Set<Integer> activeTemps = new HashSet<>();

        while (!queue.isEmpty()) {
            Statement st = queue.poll();
            st.inQueue = false;
            for (Statement pst : st.getRunFroms()) {
                activeTemps.clear();
                activeTemps.addAll(st.getActiveTemps());
                if (st.getActiveTemps().contains(pst.getRenewTemp())) {
                    pst.requireResult = true;
                }
                activeTemps.remove(pst.getRenewTemp());
                activeTemps.addAll(pst.getUsedTemps());
                if (pst.mergeActiveTemps(activeTemps)) {
                    if (!pst.inQueue) {
                        pst.inQueue = true;
                        queue.add(pst);
                    }
                }
            }
        }
    }

    public boolean cleanUnusedStatements() {
        return statements.removeIf(st -> (((st instanceof Allocate) ||
                (st instanceof AllocateImm) ||
                (st instanceof Move) ||
                (st instanceof MoveImm) ||
                (st instanceof Op) ||
                (st instanceof OpImm) ||
                (st instanceof Load))
                && !st.requireResult)
                || ((st instanceof NoopWithLabel) && !st.labelJumpReachable)
        );
    }

    public void checkLiveRange() {
        Logger.log("** Processing register allocation");
        int n = statements.size();
        for (int i = 0; i < n; ++i) {
            Statement st = statements.get(i);
            Logger.log(String.format(" %04d: %s", i, st.toString()));
            for (int t : st.getActiveTemps()) {
                LiveRange r = ranges.get(t);
                if (r == null) {
                    r = new LiveRange();
                    r.first = r.last = st;
                    r.start = r.end = i;
                    ranges.put(t, r);
                } else {
                    r.last = st;
                    r.end = i;
                }
            }
        }
        Logger.log("** Live range analysis result");
        ranges.forEach((k, v) ->
                Logger.log(String.format(" Temp %d: %d(%s) -> %d(%s)",
                        k, v.start, v.first.toString(), v.end, v.last.toString())));
    }

    private static String getAvail(boolean[] a1, boolean[] a2, String p1, String p2) {
        for (int i = 0; i < a1.length; ++i) {
            if (a1[i]) {
                a1[i] = false;
                return p1 + i;
            }
        }
        for (int i = 0; i < a2.length; ++i) {
            if (a2[i]) {
                a2[i] = false;
                return p2 + i;
            }
        }
        return null;
    }

    private static void setAvail(boolean[] s, boolean[] t, String x) {
        char[] ch = x.toCharArray();
        if (ch[0] == 't') {
            t[ch[1] - '0'] = true;
        } else if (ch[0] == 's') {
            s[ch[1] - '0'] = true;
        }
    }

    public void linearRegAlloc() {
        boolean[] sAvail = new boolean[8];
        boolean[] tAvail = new boolean[10];
        IntStream.range(0, sAvail.length).forEach(i -> sAvail[i] = true);
        IntStream.range(0, tAvail.length).forEach(i -> tAvail[i] = true);
        Set<Integer> callAffected = new HashSet<>();
        for (Statement st : statements) {
            if (st instanceof CallBase) {
                callAffected.addAll(st.getActiveTemps());
                int ac = ((CallBase) st).getArgs().size();
                if (ac > maxArgCount) maxArgCount = ac;
            }
        }
        for (Statement st : statements) {
            for (int t : st.getActiveTemps()) {
                LiveRange r = ranges.get(t);
                if (r.first == st) {
                    String reg = callAffected.contains(t)
                            ? getAvail(sAvail, tAvail, "s", "t")
                            : getAvail(tAvail, sAvail, "t", "s");
                    if (reg != null) {
                        regsMap.put(t, reg);
                    } else {
                        regsMap.put(t, "v0"); // placeholder
                        if (t >= 4 && t < argCount) {
                            spillMap.put(t, t - 4);
                        } else {
                            spillMap.put(t, spilledIdNext++);
                        }
                    }
                }
            }
            for (int t : st.getActiveTemps()) {
                LiveRange r = ranges.get(t);
                if (r.last == st) {
                    setAvail(sAvail, tAvail, regsMap.get(t));
                }
            }
        }
        saveIdNext = spilledIdNext;
        for (Statement st : statements) {
            for (int t : st.getActiveTemps()) {
                String reg = regsMap.get(t);
                char[] ch = reg.toCharArray();
                if (ch[0] == 's' || (ch[0] == 't' && callAffected.contains(t))) {
                    if (!saveMap.containsKey(reg))
                        saveMap.put(reg, saveIdNext++);
                }
            }
        }
        Logger.log("** Register allocation result:");
        Logger.log(" -- Registers");
        regsMap.forEach((t, reg) ->
                Logger.log(String.format(" %d: %s",
                        t,
                        reg.equals("v0") ? ("spill " + spillMap.get(t)) : reg
                ))
        );
        Logger.log(" -- Saved registers");
        saveMap.forEach((reg, id) ->
                Logger.log(String.format(" %s: spill %d", reg, id)));
    }

    public String getRegLoad(GenState gen, int t, String regSpill) {
        String reg = regsMap.get(t);
        if (reg.equals("v0")) {
            gen.generateCode(String.format("ALOAD %s SPILLEDARG %d", regSpill, spillMap.get(t)));
            return regSpill;
        }
        return reg;
    }

    public String getRegStore(int t, Statement st) {
        if (st != null && !st.requireResult) return null;
        String reg = regsMap.get(t);
        if (reg == null && st != null) {
            throw new InternalCompilerError("unassigned register");
        }
        if (reg != null && !reg.equals("v0") && st != null) {
            for (int ct : st.getActiveTemps()) {
                if (ct == t) continue;
                if (reg.equals(regsMap.get(ct)) && ranges.get(ct).last != st) {
                    throw new InternalCompilerError("register conflict");
                }
            }
        }
        return reg;
    }

    public void doneRegStore(GenState gen, int t, String reg) {
        if (reg.equals("v0")) {
            gen.generateCode(String.format("ASTORE SPILLEDARG %d v0", spillMap.get(t)));
        }
    }

    public void genCode(GenState gen) {
        gen.generateCode(String.format("%s [%d] [%d] [%d]",
                name, argCount, saveIdNext, maxArgCount));
        ++gen.indentDepth;
        saveMap.forEach((reg, id) -> {
            if (reg.toCharArray()[0] == 's')
                gen.generateCode(String.format("ASTORE SPILLEDARG %d %s", id, reg));
        });
        for (int i = 0; i < argCount; ++i) {
            String reg = getRegStore(i, null);
            if (reg != null) {
                if (i < 4) {
                    gen.generateCode(String.format("MOVE %s a%d", reg, i));
                    doneRegStore(gen, i, reg);
                } else if (!reg.equals("v0")) {
                    gen.generateCode(String.format("ALOAD %s SPILLEDARG %d", reg, i - 4));
                }
            }
        }
        statements.forEach(st -> st.generateCode(gen, this));
        saveMap.forEach((reg, id) -> {
            if (reg.toCharArray()[0] == 's')
                gen.generateCode(String.format("ALOAD %s SPILLEDARG %d", reg, id));
        });
        --gen.indentDepth;
        gen.generateCode("END");
        gen.generateCode("");
    }
}
