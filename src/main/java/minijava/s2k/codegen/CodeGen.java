package minijava.s2k.codegen;

import minijava.s2k.jtb.syntaxtree.*;
import minijava.s2k.jtb.visitor.GJVoidDepthFirst;
import minijava.s2k.statement.*;

public class CodeGen extends GJVoidDepthFirst<GenState> {
    /**
     * f0 -> "MAIN"
     * f1 -> StmtList()
     * f2 -> "END"
     * f3 -> ( Procedure() )*
     * f4 -> <EOF>
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(Goal n, GenState argu) {
        argu.resetLabel();
        argu.fn.add(argu.currentFunc = new Func("MAIN", 0));
        n.f1.accept(this, argu);
        argu.currentFunc = null;
        n.f3.accept(this, argu);
    }

    /**
     * f0 -> Label()
     * f1 -> "["
     * f2 -> IntegerLiteral()
     * f3 -> "]"
     * f4 -> StmtExp()
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(Procedure n, GenState argu) {
        int argCount = Integer.parseInt(n.f2.f0.tokenImage);
        String fnName = "F_" + n.f0.f0.tokenImage;
        argu.resetLabel();
        argu.fn.add(argu.currentFunc = new Func(fnName, argCount));
        n.f4.accept(this, argu);
        argu.currentFunc = null;
    }

    /**
     * f0 -> "NOOP"
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(NoOpStmt n, GenState argu) {
        // do nothing
    }

    /**
     * f0 -> "ERROR"
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(ErrorStmt n, GenState argu) {
        argu.currentFunc.statements.add(new ErrorSt());
    }

    /**
     * f0 -> "CJUMP"
     * f1 -> Temp()
     * f2 -> Label()
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(CJumpStmt n, GenState argu) {
        int condTemp = Integer.parseInt(n.f1.f1.f0.tokenImage);
        String lg = argu.globalLabel(n.f2.f0.tokenImage);
        argu.currentFunc.statements.add(new Cond(condTemp, lg));
    }

    /**
     * f0 -> "JUMP"
     * f1 -> Label()
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(JumpStmt n, GenState argu) {
        String lg = argu.globalLabel(n.f1.f0.tokenImage);
        argu.currentFunc.statements.add(new Jump(lg));
    }

    /**
     * f0 -> "HSTORE"
     * f1 -> Temp()
     * f2 -> IntegerLiteral()
     * f3 -> Temp()
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(HStoreStmt n, GenState argu) {
        int dstTemp = Integer.parseInt(n.f1.f1.f0.tokenImage);
        int offset = Integer.parseInt(n.f2.f0.tokenImage);
        int srcTemp = Integer.parseInt(n.f3.f1.f0.tokenImage);
        argu.currentFunc.statements.add(new Store(dstTemp, offset, srcTemp));
    }

    /**
     * f0 -> "HLOAD"
     * f1 -> Temp()
     * f2 -> Temp()
     * f3 -> IntegerLiteral()
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(HLoadStmt n, GenState argu) {
        int dstTemp = Integer.parseInt(n.f1.f1.f0.tokenImage);
        int srcTemp = Integer.parseInt(n.f2.f1.f0.tokenImage);
        int offset = Integer.parseInt(n.f3.f0.tokenImage);
        argu.currentFunc.statements.add(new Load(dstTemp, srcTemp, offset));
    }

    /**
     * f0 -> "MOVE"
     * f1 -> Temp()
     * f2 -> Exp()
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(MoveStmt n, GenState argu) {
        argu.setMoveDest(Integer.parseInt(n.f1.f1.f0.tokenImage));
        n.f2.accept(this, argu);
        argu.ensureDestConsumed();
    }

    /**
     * f0 -> "PRINT"
     * f1 -> SimpleExp()
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(PrintStmt n, GenState argu) {
        n.f1.accept(this, argu);
        if (argu.isImm()) {
            argu.currentFunc.statements.add(new PrintImm(argu.getLastImm()));
        } else {
            argu.currentFunc.statements.add(new Print(argu.getLastTemp()));
        }
    }

    /**
     * f0 -> "BEGIN"
     * f1 -> StmtList()
     * f2 -> "RETURN"
     * f3 -> SimpleExp()
     * f4 -> "END"
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(StmtExp n, GenState argu) {
        n.f1.accept(this, argu);
        n.f3.accept(this, argu);
        if (argu.isImm()) {
            argu.currentFunc.statements.add(new ReturnImm(argu.getLastImm()));
        } else {
            argu.currentFunc.statements.add(new Return(argu.getLastTemp()));
        }
    }

    /**
     * f0 -> "CALL"
     * f1 -> SimpleExp()
     * f2 -> "("
     * f3 -> ( Temp() )*
     * f4 -> ")"
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(Call n, GenState argu) {
        int dstTemp = argu.getMoveDest();
        CallBase call;
        n.f1.accept(this, argu);
        if (argu.isImm()) {
            call = new CallStatic(dstTemp, argu.getLastImm());
        } else {
            call = new CallInstance(dstTemp, argu.getLastTemp());
        }
        n.f3.nodes.forEach(a -> {
            argu.requireExp = true;
            a.accept(this, argu);
            argu.requireExp = false;
            call.addArgument(argu.getLastTemp());
        });
        argu.currentFunc.statements.add(call);
    }

    /**
     * f0 -> "HALLOCATE"
     * f1 -> SimpleExp()
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(HAllocate n, GenState argu) {
        int dstTemp = argu.getMoveDest();
        n.f1.accept(this, argu);
        if (argu.isImm()) {
            argu.currentFunc.statements.add(new AllocateImm(dstTemp, argu.getLastImm()));
        } else {
            argu.currentFunc.statements.add(new Allocate(dstTemp, argu.getLastTemp()));
        }
    }

    /**
     * f0 -> Operator()
     * f1 -> Temp()
     * f2 -> SimpleExp()
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(BinOp n, GenState argu) {
        int dstTemp = argu.getMoveDest();
        String op = n.f0.f0.choice.toString();
        int lhsTemp = Integer.parseInt(n.f1.f1.f0.tokenImage);
        n.f2.accept(this, argu);
        if (argu.isImm()) {
            argu.currentFunc.statements.add(new OpImm(dstTemp, lhsTemp, argu.getLastImm(), op));
        } else {
            argu.currentFunc.statements.add(new Op(dstTemp, lhsTemp, argu.getLastTemp(), op));
        }
    }

    /**
     * f0 -> Temp()
     * | IntegerLiteral()
     * | Label()
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(SimpleExp n, GenState argu) {
        argu.requireExp = true;
        n.f0.accept(this, argu);
        argu.requireExp = false;
    }

    /**
     * f0 -> "TEMP"
     * f1 -> IntegerLiteral()
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(Temp n, GenState argu) {
        if (!argu.requireExp)
            throw new InternalCompilerError("unexpected exp");

        int id = Integer.parseInt(n.f1.f0.tokenImage);
        if (argu.isMove()) {
            int dstTemp = argu.getMoveDest();
            argu.currentFunc.statements.add(new Move(dstTemp, id));
        } else {
            argu.setResultTemp(id);
        }
    }

    /**
     * f0 -> <INTEGER_LITERAL>
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(IntegerLiteral n, GenState argu) {
        if (!argu.requireExp)
            throw new InternalCompilerError("unexpected exp");

        if (argu.isMove()) {
            int dstTemp = argu.getMoveDest();
            argu.currentFunc.statements.add(new MoveImm(dstTemp, n.f0.tokenImage));
        } else {
            argu.setResultImm(n.f0.tokenImage);
        }
    }

    /**
     * f0 -> <IDENTIFIER>
     *
     * @param n    node
     * @param argu environment
     */
    @Override
    public void visit(Label n, GenState argu) {
        if (argu.requireExp) {
            String fnName = "F_" + n.f0.tokenImage;
            if (argu.isMove()) {
                // assume label exp is function
                int dstTemp = argu.getMoveDest();
                argu.currentFunc.statements.add(new MoveImm(dstTemp, fnName));
            } else {
                argu.setResultImm(fnName);
            }
        } else {
            String lg = argu.globalLabel(n.f0.tokenImage);
            argu.currentFunc.statements.add(new NoopWithLabel(lg));
        }
    }
}
