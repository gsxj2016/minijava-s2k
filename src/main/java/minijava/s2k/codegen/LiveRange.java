package minijava.s2k.codegen;

import minijava.s2k.statement.Statement;

public class LiveRange {
    public Statement first;
    public Statement last;
    public int start;
    public int end;
}
