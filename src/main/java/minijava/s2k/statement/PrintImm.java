package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

public class PrintImm extends Statement {
    private final String shown;

    public PrintImm(String shown) {
        this.shown = shown;
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        gen.generateCode("PRINT " + shown);
    }

    @Override
    public String toString() {
        return "PRINT Imm:" + shown;
    }
}
