package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

import java.util.Collections;
import java.util.List;

public class Print extends Statement {
    private final int shown;

    public Print(int shown) {
        this.shown = shown;
    }

    @Override
    public List<Integer> getUsedTemps() {
        return Collections.singletonList(shown);
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        String reg = fn.getRegLoad(gen, shown, "v0");
        gen.generateCode("PRINT " + reg);
    }

    @Override
    public String toString() {
        return "PRINT " + shown;
    }
}
