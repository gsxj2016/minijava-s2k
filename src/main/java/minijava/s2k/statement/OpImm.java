package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

import java.util.Collections;
import java.util.List;

public class OpImm extends Statement {
    private final int dst, lhs;
    private final String rhs;
    private final String op;

    public OpImm(int dst, int lhs, String rhs, String op) {
        this.dst = dst;
        this.lhs = lhs;
        this.rhs = rhs;
        this.op = op;
    }

    @Override
    public List<Integer> getUsedTemps() {
        return Collections.singletonList(lhs);
    }

    @Override
    public Integer getRenewTemp() {
        return dst;
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        String regDst = fn.getRegStore(dst, this);
        if (regDst != null) {
            String regLhs = fn.getRegLoad(gen, lhs, "v0");
            gen.generateCode(String.format("MOVE %s %s %s %s", regDst, op, regLhs, rhs));
            fn.doneRegStore(gen, dst, regDst);
        }
    }

    @Override
    public String toString() {
        return op + " " + dst + " <- " + lhs + ", Imm:" + rhs;
    }
}
