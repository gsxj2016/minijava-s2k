package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

public class AllocateImm extends Statement {
    private final int dst;
    private final String size;

    public AllocateImm(int dst, String size) {
        this.dst = dst;
        this.size = size;
    }

    @Override
    public Integer getRenewTemp() {
        return dst;
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        String regDst = fn.getRegStore(dst, this);
        if (regDst != null) {
            gen.generateCode(String.format("MOVE %s HALLOCATE %s", regDst, size));
            fn.doneRegStore(gen, dst, regDst);
        }
    }

    @Override
    public String toString() {
        return "HALLOCATE " + dst + " <- Imm:" + size;
    }
}
