package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

import java.util.stream.Collectors;

public class CallStatic extends CallBase {
    private final int dst;
    private final String fn;

    public CallStatic(int dst, String fn) {
        this.dst = dst;
        this.fn = fn;
    }

    @Override
    public Integer getRenewTemp() {
        return dst;
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        genArgPassCode(gen, fn);
        gen.generateCode("CALL " + this.fn);
        genArgEndCode(gen, fn);
        String regDst = fn.getRegStore(dst, this);
        if (regDst != null) {
            if (!regDst.equals("v0"))
                gen.generateCode(String.format("MOVE %s v0", regDst));
            fn.doneRegStore(gen, dst, regDst);
        }
    }

    @Override
    public String toString() {
        return "CALL " + dst + " <- Imm:" + fn + " ( "
                + getArgs().stream().map(Object::toString).collect(Collectors.joining(", "))
                + " )";
    }
}
