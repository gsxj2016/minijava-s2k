package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

import java.util.Collections;
import java.util.List;

public class Cond extends Statement {
    private final int cond;
    private final String target;

    public Cond(int cond, String target) {
        this.cond = cond;
        this.target = target;
    }

    @Override
    public List<Integer> getUsedTemps() {
        return Collections.singletonList(cond);
    }

    @Override
    public String getJumpTarget() {
        return target;
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        String reg = fn.getRegLoad(gen, cond, "v0");
        gen.generateCode(String.format("CJUMP %s %s", reg, target));
    }

    @Override
    public String toString() {
        return "CJUMP " + cond + " :" + target;
    }
}
