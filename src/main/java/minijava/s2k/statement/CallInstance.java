package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

import java.util.stream.Collectors;

public class CallInstance extends CallBase {
    private final int dst, fn;

    public CallInstance(int dst, int fn) {
        this.dst = dst;
        this.fn = fn;
        this.used.add(fn);
    }

    @Override
    public Integer getRenewTemp() {
        return dst;
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        genArgPassCode(gen, fn);
        String regFn = fn.getRegLoad(gen, this.fn, "v1");
        gen.generateCode("CALL " + regFn);
        genArgEndCode(gen, fn);
        String regDst = fn.getRegStore(dst, this);
        if (regDst != null) {
            if (!regDst.equals("v0"))
                gen.generateCode(String.format("MOVE %s v0", regDst));
            fn.doneRegStore(gen, dst, regDst);
        }
    }

    @Override
    public String toString() {
        return "CALL " + dst + " <- " + fn + " ( "
                + getArgs().stream().map(Object::toString).collect(Collectors.joining(", "))
                + " )";
    }
}
