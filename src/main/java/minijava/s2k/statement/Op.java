package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

import java.util.Arrays;
import java.util.List;

public class Op extends Statement {
    private final int dst, lhs, rhs;
    private final String op;

    public Op(int dst, int lhs, int rhs, String op) {
        this.dst = dst;
        this.lhs = lhs;
        this.rhs = rhs;
        this.op = op;
    }

    @Override
    public List<Integer> getUsedTemps() {
        return Arrays.asList(lhs, rhs);
    }

    @Override
    public Integer getRenewTemp() {
        return dst;
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        String regDst = fn.getRegStore(dst, this);
        if (regDst != null) {
            String regLhs = fn.getRegLoad(gen, lhs, "v0");
            String regRhs = fn.getRegLoad(gen, rhs, "v1");
            gen.generateCode(String.format("MOVE %s %s %s %s", regDst, op, regLhs, regRhs));
            fn.doneRegStore(gen, dst, regDst);
        }
    }

    @Override
    public String toString() {
        return op + " " + dst + " <- " + lhs + ", " + rhs;
    }
}
