package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

public class MoveImm extends Statement {
    private final int dst;
    private final String src;

    public MoveImm(int dst, String src) {
        this.dst = dst;
        this.src = src;
    }

    @Override
    public Integer getRenewTemp() {
        return dst;
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        String regDst = fn.getRegStore(dst, this);
        if (regDst != null) {
            gen.generateCode(String.format("MOVE %s %s", regDst, src));
            fn.doneRegStore(gen, dst, regDst);
        }
    }

    @Override
    public String toString() {
        return "MOVE " + dst + " <- Imm:" + src;
    }
}
