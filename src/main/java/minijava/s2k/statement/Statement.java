package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

import java.util.*;

public abstract class Statement {

    public boolean labelJumpReachable = false;
    public boolean requireResult = false;
    public boolean reachable = false;
    public boolean inQueue = false;
    private final List<Statement> runFroms = new ArrayList<>();
    private final Set<Integer> activeTemps = new HashSet<>();

    public List<Integer> getUsedTemps() {
        return Collections.emptyList();
    }

    public Integer getRenewTemp() {
        return null;
    }

    public String getStatementLabel() {
        return null;
    }

    public String getJumpTarget() {
        return null;
    }

    public boolean canRunNextStatement() {
        return true;
    }

    public final List<Statement> getRunFroms() {
        return Collections.unmodifiableList(runFroms);
    }

    public final void addRunFrom(Statement st) {
        runFroms.add(st);
    }

    public final void resetRunFroms() {
        labelJumpReachable = false;
        runFroms.clear();
    }

    public final Set<Integer> getActiveTemps() {
        return Collections.unmodifiableSet(activeTemps);
    }

    public final boolean mergeActiveTemps(Set<Integer> temps) {
        return activeTemps.addAll(temps);
    }

    public final void resetActiveTemps() {
        activeTemps.clear();
        activeTemps.addAll(getUsedTemps());
    }

    public abstract void generateCode(GenState gen, Func fn);

}
