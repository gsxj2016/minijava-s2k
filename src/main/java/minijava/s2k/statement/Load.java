package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

import java.util.Collections;
import java.util.List;

public class Load extends Statement {
    private final int dst, src, offset;

    public Load(int dst, int src, int offset) {
        this.dst = dst;
        this.src = src;
        this.offset = offset;
    }

    @Override
    public List<Integer> getUsedTemps() {
        return Collections.singletonList(src);
    }

    @Override
    public Integer getRenewTemp() {
        return dst;
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        String regDst = fn.getRegStore(dst, this);
        if (regDst != null) {
            String regSrc = fn.getRegLoad(gen, src, "v1");
            gen.generateCode(String.format("HLOAD %s %s %d", regDst, regSrc, offset));
            fn.doneRegStore(gen, dst, regDst);
        }
    }

    @Override
    public String toString() {
        return "LOAD " + dst + " <- " + src + " [" + offset + "]";
    }
}
