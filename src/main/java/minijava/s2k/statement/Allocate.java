package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

import java.util.Collections;
import java.util.List;

public class Allocate extends Statement {
    private final int dst, size;

    public Allocate(int dst, int size) {
        this.dst = dst;
        this.size = size;
    }

    @Override
    public List<Integer> getUsedTemps() {
        return Collections.singletonList(size);
    }

    @Override
    public Integer getRenewTemp() {
        return dst;
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        String regDst = fn.getRegStore(dst, this);
        if (regDst != null) {
            String regSize = fn.getRegLoad(gen, size, "v1");
            gen.generateCode(String.format("MOVE %s HALLOCATE %s", regDst, regSize));
            fn.doneRegStore(gen, dst, regDst);
        }
    }

    @Override
    public String toString() {
        return "HALLOCATE " + dst + " <- " + size;
    }
}
