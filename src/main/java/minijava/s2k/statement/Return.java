package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

import java.util.Collections;
import java.util.List;

public class Return extends Statement {
    private final int result;

    public Return(int result) {
        this.result = result;
    }

    @Override
    public List<Integer> getUsedTemps() {
        return Collections.singletonList(result);
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        String reg = fn.getRegLoad(gen, result, "v0");
        if (!reg.equals("v0")) {
            gen.generateCode("MOVE v0 " + reg);
        }
    }

    @Override
    public String toString() {
        return "RETURN " + result;
    }
}
