package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

public class ErrorSt extends Statement {
    @Override
    public boolean canRunNextStatement() {
        return false;
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        gen.generateCode("ERROR");
    }

    @Override
    public String toString() {
        return "ERROR";
    }
}
