package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

import java.util.Collections;
import java.util.List;

public class Move extends Statement {
    private final int dst, src;

    public Move(int dst, int src) {
        this.dst = dst;
        this.src = src;
    }

    @Override
    public List<Integer> getUsedTemps() {
        return Collections.singletonList(src);
    }

    @Override
    public Integer getRenewTemp() {
        return dst;
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        if (src != dst) {
            String regDst = fn.getRegStore(dst, this);
            if (regDst != null) {
                String regSrc = fn.getRegLoad(gen, src, "v0");
                if (!regDst.equals(regSrc)) {
                    gen.generateCode(String.format("MOVE %s %s", regDst, regSrc));
                }
                fn.doneRegStore(gen, dst, regDst);
            }
        }
    }

    @Override
    public String toString() {
        return "MOVE " + dst + " <- " + src;
    }
}
