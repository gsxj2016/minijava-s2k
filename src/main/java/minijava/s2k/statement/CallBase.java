package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public abstract class CallBase extends Statement {
    private final ArrayList<Integer> args = new ArrayList<>();
    final List<Integer> used = new ArrayList<>();

    public void addArgument(int a) {
        args.add(a);
        used.add(a);
    }

    @Override
    public final List<Integer> getUsedTemps() {
        return Collections.unmodifiableList(used);
    }

    public final List<Integer> getArgs() {
        return Collections.unmodifiableList(args);
    }

    private List<String> getAffectedTempRegs(Func fn) {
        return getActiveTemps().stream()
                .filter(t -> fn.ranges.get(t).last != this)
                .map(fn.regsMap::get)
                .filter(r -> r.toCharArray()[0] == 't')
                .distinct()
                .collect(Collectors.toList());
    }

    void genArgPassCode(GenState gen, Func fn) {
        // save caller-saved regs
        List<String> tRegs = getAffectedTempRegs(fn);
        tRegs.forEach(reg -> {
            int id = fn.saveMap.get(reg);
            gen.generateCode(String.format("ASTORE SPILLEDARG %d %s", id, reg));
        });
        // pass arguments
        int ac = args.size();
        for (int i = 0; i < ac; ++i) {
            int t = args.get(i);
            String reg = fn.getRegLoad(gen, t, "v0");
            if (i < 4) {
                gen.generateCode(String.format("MOVE a%d %s", i, reg));
            } else {
                gen.generateCode(String.format("PASSARG %d %s", i - 3, reg));
            }
        }
    }

    void genArgEndCode(GenState gen, Func fn) {
        // restore caller-saved regs
        List<String> tRegs = getAffectedTempRegs(fn);
        tRegs.forEach(reg -> {
            int id = fn.saveMap.get(reg);
            gen.generateCode(String.format("ALOAD %s SPILLEDARG %d", reg, id));
        });
    }
}
