package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

public class ReturnImm extends Statement {
    private final String result;

    public ReturnImm(String result) {
        this.result = result;
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        gen.generateCode("MOVE v0 " + result);
    }

    @Override
    public String toString() {
        return "RETURN Imm:" + result;
    }
}
