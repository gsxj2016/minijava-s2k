package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

public class Jump extends Statement {
    private final String target;

    public Jump(String target) {
        this.target = target;
    }

    @Override
    public String getJumpTarget() {
        return target;
    }

    @Override
    public boolean canRunNextStatement() {
        return false;
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        gen.generateCode("JUMP " + target);
    }

    @Override
    public String toString() {
        return "JUMP :" + target;
    }
}
