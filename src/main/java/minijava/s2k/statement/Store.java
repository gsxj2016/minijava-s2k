package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

import java.util.Arrays;
import java.util.List;

public class Store extends Statement {
    private final int dst, offset, src;

    public Store(int dst, int offset, int src) {
        this.dst = dst;
        this.offset = offset;
        this.src = src;
    }

    @Override
    public List<Integer> getUsedTemps() {
        return Arrays.asList(dst, src);
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        String regSrc = fn.getRegLoad(gen, src, "v1");
        String regDst = fn.getRegLoad(gen, dst, "v0");
        gen.generateCode(String.format("HSTORE %s %d %s", regDst, offset, regSrc));
    }

    @Override
    public String toString() {
        return "STORE " + dst + " [" + offset + "] <- " + src;
    }
}
