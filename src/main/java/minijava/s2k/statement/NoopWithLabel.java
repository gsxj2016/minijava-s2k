package minijava.s2k.statement;

import minijava.s2k.codegen.Func;
import minijava.s2k.codegen.GenState;

public class NoopWithLabel extends Statement {
    private final String l;

    public NoopWithLabel(String l) {
        this.l = l;
    }

    @Override
    public String getStatementLabel() {
        return l;
    }

    @Override
    public void generateCode(GenState gen, Func fn) {
        gen.generateCode(l + "   NOOP");
    }

    @Override
    public String toString() {
        return ">" + l + ":";
    }
}
