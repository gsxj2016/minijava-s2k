//
// Generated by JTB 1.3.2
//

package minijava.s2k.jtb.syntaxtree;

/**
 * Grammar production:
 * f0 -> ( ( Label() )? Stmt() )*
 */
public class StmtList implements Node {
   public NodeListOptional f0;

   public StmtList(NodeListOptional n0) {
      f0 = n0;
   }

   public void accept(minijava.s2k.jtb.visitor.Visitor v) {
      v.visit(this);
   }
   public <R,A> R accept(minijava.s2k.jtb.visitor.GJVisitor<R,A> v, A argu) {
      return v.visit(this,argu);
   }
   public <R> R accept(minijava.s2k.jtb.visitor.GJNoArguVisitor<R> v) {
      return v.visit(this);
   }
   public <A> void accept(minijava.s2k.jtb.visitor.GJVoidVisitor<A> v, A argu) {
      v.visit(this,argu);
   }
}

