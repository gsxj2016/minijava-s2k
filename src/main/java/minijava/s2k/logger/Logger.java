package minijava.s2k.logger;

public class Logger {
    public static boolean logging = false;

    public static void log(String msg) {
        if (logging) {
            System.err.println(">>>> DEBUG: " + msg);
        }
    }
}
