import minijava.s2k.codegen.CodeGen;
import minijava.s2k.codegen.GenState;
import minijava.s2k.jtb.ParseException;
import minijava.s2k.jtb.SpigletParser;
import minijava.s2k.jtb.Token;
import minijava.s2k.jtb.syntaxtree.Node;
import minijava.s2k.logger.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Main {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Error: No input file.");
            System.exit(2);
        }

        if ("1".equals(System.getProperty("minijava_debug_log"))) {
            Logger.logging = true;
        }

        try (InputStream in = new FileInputStream(args[0])) {
            Node root = new SpigletParser(in).Goal();
            GenState state = new GenState();
            root.accept(new CodeGen(), state);
            state.genCode();
            System.out.print(state.code.toString());
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(2);
        } catch (ParseException e) {
            Token token = e.currentToken;
            System.err.printf("Syntax error at %d:%d.\n", token.beginLine, token.beginColumn);
            System.exit(1);
        }
    }
}
